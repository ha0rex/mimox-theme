<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
/**
 * Outputs summary about the post's author
 *
 * @action Before the template: 'us_before_template:templates/blog/single-post-author'
 * @action After the template: 'us_after_template:templates/blog/single-post-author'
 */
global $authordata;
global $show_long_description;
//print_r($authordata);
$author_avatar = get_avatar( $authordata->ID );
$author_url = get_the_author_meta( 'url' );
if ( ! empty( $author_url ) ) {
	$author_avatar = '<a href="' . esc_url( $author_url ) . '" rel="author external" target="_blank">' . $author_avatar . '</a>';
}
?>

<section class="l-section for_author">
	<div class="l-section-h i-cf">
		<div class="w-author" itemscope="itemscope" itemtype="https://schema.org/Person" itemprop="author">
			<div class="w-author-img">
				<a class="linkedin" href="<?php echo esc_url( get_the_author_meta( 'linkedin' ) ) ?>" target="blank">
					<?php echo $author_avatar ?>
				</a>
			</div>
			<div class="socials">
				<?php if( get_the_author_meta( 'linkedin' ) ): ?>
					<a class="linkedin" href="<?php echo esc_url( get_the_author_meta( 'linkedin' ) ) ?>" target="blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
				<?php endif; ?>
			</div>
			<div class="w-author-name" itemprop="name">
				<?php the_author(); ?>
			</div>
			<div class="w-author-desc" itemprop="description"><?php echo $show_long_description ? str_replace('|', '', get_the_author_meta( 'description' )) : explode('|', get_the_author_meta( 'description' ))[0] ?></div>
			<div class="w-author-contact">
				<?php if( get_the_author_meta( 'phone' ) ): ?>
					<div class="w-author-contact-detail w-author-phone">
						<span class="title"><?php _e('Phone', 'Mimox') ?></span>
						<span class="phone"><a href="tel:<?php echo get_the_author_meta( 'phone' ); ?>"><?php echo get_the_author_meta( 'phone' ); ?></a></span>
					</div>
				<?php endif; ?>
				<div class="w-author-contact-detail w-author-email">
					<span class="title"><?php _e('E-mail', 'Mimox') ?></span>
					<div class="w-btn-wrapper align_left">
						<a class="w-btn style_raised color_white icon_none email" href="mailto:<?php echo $authordata->user_email ?>" style="font-size: 14px;">
							<span class="w-btn-label"><?php echo $authordata->user_email ?></span><span class="ripple-container"></span>
						</a>
					</div>
				</div>
				<?php if( get_the_author_meta( 'skype' ) ): ?>
				<div class="w-author-contact-detail w-author-skype">
					<span class="title"><?php _e('Skype', 'Mimox') ?></span>
					<div class="w-btn-wrapper align_left">
						<a class="w-btn style_raised color_white icon_none email" href="skype:<?php echo get_the_author_meta( 'skype' ); ?>" style="font-size: 14px;">
							<span class="w-btn-label"><?php echo get_the_author_meta( 'skype' ); ?></span><span class="ripple-container"></span>
						</a>
					</div>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
