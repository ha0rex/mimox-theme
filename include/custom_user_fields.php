<?php

// Hooks near the bottom of profile page (if current user) 
add_action('show_user_profile', 'mimox_custom_user_profile_fields');

// Hooks near the bottom of the profile page (if not current user) 
add_action('edit_user_profile', 'mimox_custom_user_profile_fields');

// @param WP_User $user
function mimox_custom_user_profile_fields( $user ) {
?>
    <table class="form-table">
        <tr>
            <th>
                <label for="code"><?php _e( 'Phone', 'Mimox' ); ?></label>
            </th>
            <td>
                <input type="text" name="phone" id="phone" value="<?php echo esc_attr( get_the_author_meta( 'phone', $user->ID ) ); ?>" class="regular-text" />
            </td>
        </tr>
        <tr>
            <th>
                <label for="code"><?php _e( 'Skype', 'Mimox' ); ?></label>
            </th>
            <td>
                <input type="text" name="skype" id="skype" value="<?php echo esc_attr( get_the_author_meta( 'skype', $user->ID ) ); ?>" class="regular-text" />
            </td>
        </tr>
        <tr>
            <th>
                <label for="code"><?php _e( 'LinkedIn', 'Mimox' ); ?></label>
            </th>
            <td>
                <input type="text" name="linkedin" id="linkedin" value="<?php echo esc_attr( get_the_author_meta( 'linkedin', $user->ID ) ); ?>" class="regular-text" />
            </td>
        </tr>
    </table>
<?php
}


// Hook is used to save custom fields that have been added to the WordPress profile page (if current user) 
add_action( 'personal_options_update', 'mimox_update_extra_profile_fields' );

// Hook is used to save custom fields that have been added to the WordPress profile page (if not current user) 
add_action( 'edit_user_profile_update', 'mimox_update_extra_profile_fields' );

function mimox_update_extra_profile_fields( $user_id ) {
    if ( current_user_can( 'edit_user', $user_id ) ) {
        update_user_meta( $user_id, 'phone', $_POST['phone'] );
        update_user_meta( $user_id, 'skype', $_POST['skype'] );
        update_user_meta( $user_id, 'linkedin', $_POST['linkedin'] );
    }
}

?>