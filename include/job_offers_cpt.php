<?php
/**
 * Registers Job offers custom post type
 */
add_action( 'init', 'register_mimox_job_offers_custom_post_type_function' );
function register_mimox_job_offers_custom_post_type_function() {
  register_post_type( 'mimox_job_offers',
    array(
      'labels' => array(
        'name' => __( 'Job offers', 'Mimox' ),
        'singular_name' => __( 'Job offer', 'Mimox' ),
        'add_new' => __( 'Add new', 'Mimox' ),
        'add_new_item' => __( 'Add new job offer', 'Mimox' ),
        'edit_item' => __( 'Edit job offer', 'Mimox' ),
        'new_item' => __( 'New job offer', 'Mimox' ),
        'view_item' => __( 'View job offer', 'Mimox' ),
        'not_found' => __( 'No job offers found', 'Mimox' ),
      ),
      'public' => true,
      'has_archive' => false,
      'rewrite' => array('slug' => 'jobs', 'with_front'=>false),
      'menu_icon' => 'dashicons-portfolio',
      'taxonomies' => array('post_tag'),
      'supports' => array('title', 'editor', 'author')
    )
  );
}

add_action( 'load-post.php', 'mimox_job_offers_meta_boxes_setup' );
add_action( 'load-post-new.php', 'mimox_job_offers_meta_boxes_setup' );

function mimox_job_offers_meta_boxes_setup() {
  /* Add meta boxes on the 'add_meta_boxes' hook. */
  add_action( 'add_meta_boxes', 'mimox_job_offers_add_post_meta_boxes' );
  
  /* Save post meta on the 'save_post' hook. */
  add_action( 'save_post', 'mimox_job_offers_save_post_meta', 10, 2 );
}

function mimox_job_offers_add_post_meta_boxes() {
  add_meta_box(
    'mimox_job_offers_meta_box',      // Unique ID
    esc_html__( 'Details of job offer', 'Mimox' ),    // Title
    'mimox_job_offers_meta_box',   // Callback function
    'mimox_job_offers',         // Admin page (or post type)
    'normal',         // Context
    'high'         // Priority
  );
}

/* Display the post meta box. */
function mimox_job_offers_meta_box( $object, $box ) { ?>
	<?php $job_offer_details = get_post_meta( $object->ID, 'job_offer_details', true ); ?>
	<?php $job_offer_details = $job_offer_details ? $job_offer_details : array() ?>
  
  	<?php wp_nonce_field( basename( __FILE__ ), 'mimox_job_offers_meta_box_nonce' ); ?>
  	
  	<div class="row">
  		<label for="expected-salary"><?php _e( 'Expected salary', 'Mimox' ) ?></label>
  		<div class="fields">
			<input maxlength="8" size="8" type="text" name="job_offer_details[expected-salary-from]" id="expected-salary-from" value="<?php echo esc_attr( $job_offer_details['expected-salary-from'] ) ?>" />
			<span class="separator">-</span>
			<input maxlength="8" size="8" type="text" name="job_offer_details[expected-salary-to]" id="expected-salary-to" value="<?php echo esc_attr( $job_offer_details['expected-salary-to'] ) ?>" />
			<select name="job_offer_details[expected-salary-currency]" id="expected-salary-currency">
				<option value="HUF" <?php echo $job_offer_details['expected-salary-currency'] == 'HUF' ? 'selected' : '' ?>><?php _e( 'HUF', 'Mimox' ) ?></option>
				<option value="EUR" <?php echo $job_offer_details['expected-salary-currency'] == 'EUR' ? 'selected' : '' ?>><?php _e( 'EUR', 'Mimox' ) ?></option>
				<option value="USD" <?php echo $job_offer_details['expected-salary-currency'] == 'USD' ? 'selected' : '' ?>><?php _e( 'USD', 'Mimox' ) ?></option>
				<option value="GBP" <?php echo $job_offer_details['expected-salary-currency'] == 'GBP' ? 'selected' : '' ?>><?php _e( 'GBP', 'Mimox' ) ?></option>
			</select>
			<select name="job_offer_details[expected-salary-period]" id="expected-salary-period">
				<option value="per-month" <?php echo $job_offer_details['expected-salary-period'] == 'per-moth' ? 'selected' : '' ?>><?php _e( '/month', 'Mimox' ) ?></option>			
				<option value="per-year" <?php echo $job_offer_details['expected-salary-period'] == 'per-year' ? 'selected' : '' ?>><?php _e( '/year', 'Mimox' ) ?></option>
			</select><br />
			<textarea name="job_offer_details[expected-salary-extra]" placeholder="<?php _e( 'Extra information about the salary', 'Mimox' ) ?>"><?php echo esc_attr( $job_offer_details['expected-salary-extra'] ) ?></textarea>
  		</div>
  	</div>
  	
  	<div class="row">
  		<label for="requirements"><?php _e( 'Requirements', 'Mimox' ) ?></label>  
  		<div class="fields">
			<?php
			wp_editor( $job_offer_details['requirements'], 'requirements', array(
				'wpautop'       => false,
				'media_buttons' => true,
				'textarea_name' => 'job_offer_details[requirements]',
				'textarea_rows' => 10,
				'teeny'         => true
			) );
			?>
		</div>
  	</div>
  	
  	<div class="row">
  		<label for="place-of-work"><?php _e( 'Place of work', 'Mimox' ) ?></label>
  		<div class="fields">
			<input type="text" name="job_offer_details[place-of-work]" id="place-of-work" value="<?php echo esc_attr( $job_offer_details['place-of-work'] ) ?>" />
  		</div>
  	</div>
  	
  	<div class="row">
  		<label for="benefits"><?php _e( 'Benefits', 'Mimox' ) ?></label>  
  		<div class="fields">
			<?php
			wp_editor( $job_offer_details['benefits'], 'benefits', array(
				'wpautop'       => false,
				'media_buttons' => true,
				'textarea_name' => "job_offer_details[benefits]",
				'textarea_rows' => 10,
				'teeny'         => true
			) );
			?>
		</div>
  	</div>
  	
    <div class="row">
  		<label for="for-you-if"><?php _e( 'It is for you if', 'Mimox' ) ?></label>
  		<div class="fields">
			<textarea name="job_offer_details[for-you-if]" id="for-you-if"><?php echo esc_attr( $job_offer_details['for-you-if'] ) ?></textarea>
  		</div>
  	</div>
  	
    <div class="row">
  		<label for="dress-code"><?php _e( 'Dress code', 'Mimox' ) ?></label>
  		<div class="fields">
			<select name="job_offer_details[dress-code]" id="dress-code">
				<option value="tie" <?php echo $job_offer_details['dress-code'] == 'tie' ? 'selected' : '' ?>><?php _e( 'Tie', 'Mimox' ) ?></option>
				<option value="shirt" <?php echo $job_offer_details['dress-code'] == 'shirt' ? 'selected' : '' ?>><?php _e( 'Shirt', 'Mimox' ) ?></option>
				<option value="t-shirt" <?php echo $job_offer_details['dress-code'] == 't-shirt' ? 'selected' : '' ?>><?php _e( 'T-shirt', 'Mimox' ) ?></option>
			</select>
  		</div>
  	</div>
<?php }

/* Save the meta box's post metadata. */
function mimox_job_offers_save_post_meta( $post_id, $post ) {
	/* Verify the nonce before proceeding. */
	if ( !isset( $_POST['mimox_job_offers_meta_box_nonce'] ) || !wp_verify_nonce( $_POST['mimox_job_offers_meta_box_nonce'], basename( __FILE__ ) ) )
		return $post_id;

	/* Get the post type object. */
	$post_type = get_post_type_object( $post->post_type );

	/* Check if the current user has permission to edit the post. */
	if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
		return $post_id;


	/* Get the posted data and sanitize it for use as an HTML class. */
	//$new_meta_value = ( isset( $_POST['smashing-post-class'] ) ? sanitize_html_class( $_POST['smashing-post-class'] ) : '' );

	update_post_meta( $post_id, 'job_offer_details', $_POST['job_offer_details'] );
}

/*  Get all job offers function */

function get_all_job_offers( $limit = -1, $current = false ) {
	 $args = array(
		'posts_per_page'   => $limit,
		'post_type'        => 'mimox_job_offers',
		'post_status'      => 'publish',
	);
	
	$job_offers_array = get_posts( $args );
	
	$job_offers_html = '<ul class="job-offer-list">';
	
	foreach( $job_offers_array as $job_offer ) {
		$job_offer_details = get_post_meta( $job_offer->ID, 'job_offer_details', true );
		$job_offers_html .= '<li class="job-offer '.( strtotime( $job_offer->post_date ) > strtotime( '-14 days' ) ? 'job-offer-new' : '' ).' '.( $current == $job_offer->ID ? 'current' : '' ).'"><a href="'.get_permalink($job_offer->ID).'">'.get_the_title($job_offer->ID).'</a><span class="location">'.$job_offer_details['place-of-work'].'</span></li>';
	}
	
	$job_offers_html .= '</ul>';
	
	return $job_offers_html;
}

/*  Get all job offers (extended view) function */

function get_all_job_offers_extended( $limit = -1, $current = false ) {
	 $args = array(
		'posts_per_page'   => $limit,
		'post_type'        => 'mimox_job_offers',
		'post_status'      => 'publish',
	);
	
	$job_offers_array = get_posts( $args );
	
	$job_offers_html = '<ul class="job-offer-list job-offer-list-extended">';
	
	foreach( $job_offers_array as $job_offer ) {
		$job_offer_details = get_post_meta( $job_offer->ID, 'job_offer_details', true );
		$job_offers_html .= '<li onclick="window.location=\''.get_permalink($job_offer->ID).'\'" class="job-offer '.( strtotime( $job_offer->post_date ) > strtotime( '-14 days' ) ? 'job-offer-new' : '' ).' '.( $current == $job_offer->ID ? 'current' : '' ).'"><div class="details"><a href="'.get_permalink($job_offer->ID).'">'.get_the_title($job_offer->ID).'</a><span class="salary"><span class="label">'.__('gross', 'Mimox').'</span> '.$job_offer_details['expected-salary-from'].'-'.$job_offer_details['expected-salary-to'].' <span class="currency">'.$job_offer_details['expected-salary-currency'].'</span></span><span class="location">'.$job_offer_details['place-of-work'].'</span></div><div class="for-you-if">'.$job_offer_details['for-you-if'].'</div></li>';
	}
	
	$job_offers_html .= '</ul>';
	
	return $job_offers_html;
}


/*  Get all job offers (okonzervativ view) function */

function get_all_job_offers_okonzervativ( $limit = -1, $current = false ) {
	 $args = array(
		'posts_per_page'   => $limit,
		'post_type'        => 'mimox_job_offers',
		'post_status'      => 'publish',
	);
	
	$job_offers_array = get_posts( $args );
	
	$job_offers_html = '<div class="okonzervativ-header">
		<a class="logo" href="/">'.__('Mimox Orthodox', 'Mimox').'</a>
		<a class="back-to-home" href="/">'.__('Back <span class="desktop">to Home</span>', 'Mimox').'</a>
	</div>
	<div class="okonzervativ-container">
	<h3>'.__('Currently available job offers', 'Mimox').'</h3>';
	
	foreach( $job_offers_array as $job_offer ) {
		$job_offer_details = get_post_meta( $job_offer->ID, 'job_offer_details', true );
		$job_offers_html .= '<a href="#job-offer-'.$job_offer->ID.'">'.get_the_title($job_offer->ID).'</a><br />
		<span class="salary"><span class="label">'.__('gross', 'Mimox').'</span> '.$job_offer_details['expected-salary-from'].'-'.$job_offer_details['expected-salary-to'].' <span class="currency">'.$job_offer_details['expected-salary-currency'].'</span><br />
		<span class="location">'.$job_offer_details['place-of-work'].'</span><br /><br />';
	}
		
	foreach( $job_offers_array as $job_offer ) {
		global $post;
		$post = get_post($job_offer->ID);
		$job_offers_html .= '<br /><br />';
		$job_offers_html .= '////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////';
	
		$job_offers_html .= '<br /><br />';
	
		$job_offer_details = get_post_meta( $job_offer->ID, 'job_offer_details', true );
			
		$job_offer->post_content = str_replace('<hr />', '<br /><br />////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////<br /><br />', $job_offer->post_content);
		$job_offer_details['requirements'] = str_replace('<hr />', '<br /><br />////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////<br /><br />', $job_offer_details['requirements']);

		$forms = get_option('mimox_forms');
		
		$job_offers_html .= '
		<div class="details" id="job-offer-'.$job_offer->ID.'">
			<br />
			<strong>'.get_the_title($job_offer->ID).'</strong> // <span class="location">'.$job_offer_details['place-of-work'].'</span><br />
			<br />
			<span class="salary">
				<strong>'.__('Salary', 'Mimox').'</strong>('.__('gross', 'Mimox').' '.$job_offer_details['expected-salary-from'].'-'.$job_offer_details['expected-salary-to'].' <span class="currency">'.$job_offer_details['expected-salary-currency'].'</span>)
			</span>
		</div>
		<br />
		<div class="description">'.$job_offer->post_content.'</div>
		<br />
		<div class="requirements">'.$job_offer_details['requirements'].'</div>
		<br />
		<br />
		'.do_shortcode('[cl-popup show_on="text" btn_label="'.__('Apply now!', 'Mimox').'" align="left" size="l" paddings="" animation="fadeIn"]<div class="lightbox-job-apply">[contact-form-7 id="'.$forms['job_apply'][get_locale()].'" job_offer_id="<?php echo $post->ID ?>" title="1 kapcsolati űrlap"]</div>[/cl-popup]').'<br />
		<br />
		<a class="back-to-top" href="#">'.__('Back to top', 'Mimox').'</a>';
	}
	
	$job_offers_html .= '</ul>
	</div>';
	
	return $job_offers_html;
}
?>