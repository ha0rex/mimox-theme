<?php
function wpcf7_mimox_job_offers_list_function($choices, $args=array()) {
	global $post;
	
	 $query_args = array(
		'posts_per_page'   => $limit,
		'post_type'        => 'mimox_job_offers',
		'post_status'      => 'publish',
	);
	
	$job_offers_array = get_posts( $query_args );
	
	foreach( $job_offers_array as $job_offer ) {
		/*
		 * Current job offer should be the first in the $choices array, so it can act like the defaultly selected value
		 */
		if($post->ID == $job_offer->ID) {
			$title = get_the_title($job_offer->ID);
			$choices = array( "$title" => get_the_title($job_offer->ID) ) + $choices;
		}
		else {
			$choices[get_the_title($job_offer->ID)] = get_the_title($job_offer->ID);
		}
	}

    return $choices;
} 

add_filter('wpcf7_mimox_job_offers_list', 'wpcf7_mimox_job_offers_list_function', 10, 2);
?>