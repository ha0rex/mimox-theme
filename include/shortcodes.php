<?php
/* NET salary calculator shortcode */

function mimox_net_calculator_shortcode_func( $atts ) {
	$atts = shortcode_atts( array(
		'currency' => 'HUF',
		'no_of_children_min' => 0,
		'no_of_children_max' => 10,
		'gross_salary_default' => false,
		'no_of_children_default' => false,
		'formula' => false,
	), $atts, 'mimox_calculator' );
	
	$atts['formula'] = $atts['formula'] && $atts['formula']!='' ? $atts['formula'] : get_option('mimox_calculator_js');
	
	/*
	$no_of_children_select_options = '<option value="0" disabled '.($atts['no_of_children_default']==false ? 'selected' : '').'>'.__('No of children', 'Mimox').'</option>';
	
	for($i=$atts['no_of_children_min']; $i<=$atts['no_of_children_max']; $i++) {
		$no_of_children_select_options .= '<option value="'.$i.'" '.($atts['no_of_children_default']==$i ? 'selected' : '').'>'.$i.'</option>';
	}
	*/
	
	return '<form class="mimox-form mimox-form-calculator" method="POST" action="" data-currency="'.$atts['currency'].'" data-formula="'.$atts['formula'].'">
		<div class="fields">
		<div class="form-row"><input type="text" name="gross_salary" placeholder="'.__('Gross salary amount', 'Mimox').'" value="'.$atts['gross_salary_default'].'" /><span class="currency">'.$atts['currency'].'</span></div>
		<div class="form-row">
			<input type="text" name="no_of_children" placeholder="'.__('No of children', 'Mimox').'" value="'.$atts['no_of_children_default'].'" />
		</div>
		<div class="form-row"><a class="orange calculate">'.__('Calculate!', 'Mimox').'</a></div>
		</div>
		<div class="result"><label>'.__('Based on the given values, the NET amount is', 'Mimox').'</label><span class="value"></span></div>
	</form>
	
	<script type="text/javascript">
	jQuery(document).ready(function() {
		init_mimox_net_calculator();
	});
	</script>';
}
add_shortcode( 'mimox_calculator', 'mimox_net_calculator_shortcode_func' );

/* NET salary calculator shortcode END */

/* JOB OFFERS LIST shortcode */

function get_all_job_offers_shortcode_func( $atts ) {
	$atts = shortcode_atts( array(
		'limit' => -1,
		'current' => false,
	), $atts, 'mimox_job_offers' );
	
	return get_all_job_offers( $atts['limit'], $atts['current'] );
}

add_shortcode( 'mimox_get_all_job_offers', 'get_all_job_offers_shortcode_func' );

/* JOB OFFERS LIST shortcode END */

/* JOB OFFERS EXTENDED LIST shortcode */

function get_all_job_offers_extended_shortcode_func( $atts ) {
	$atts = shortcode_atts( array(
		'limit' => -1,
		'current' => false,
	), $atts, 'mimox_job_offers' );
	
	return get_all_job_offers_extended( $atts['limit'], $atts['current'] );
}

add_shortcode( 'mimox_get_all_job_offers_extended', 'get_all_job_offers_extended_shortcode_func' );

/* JOB OFFERS EXTENDED LIST shortcode END */


/* JOB OFFERS OKONZERVATIV LIST shortcode */

function get_all_job_offers_okonzervativ_shortcode_func( $atts ) {
	$atts = shortcode_atts( array(
		'limit' => -1,
		'current' => false,
	), $atts, 'mimox_job_offers' );
	
	return get_all_job_offers_okonzervativ( $atts['limit'], $atts['current'] );
}

add_shortcode( 'mimox_get_all_job_offers_okonzervativ', 'get_all_job_offers_okonzervativ_shortcode_func' );

/* JOB OFFERS OKONZERVATIV LIST shortcode END */


/* MIMOXINDEX shortcode */

function mimoxindex_shortcode_func( $atts ) {
	$atts = shortcode_atts( array(
		'limit' => -1,
		'speed' => 13000,
		'direction' => 'left',
		'pauseonhover' => true,
		'interval' => '-1 day',
		'comparewith' => '-1 month',
	), $atts, 'mimox_mimoxindex' );
	
	$mimox_index = mimox_index( $atts['interval'], $atts['comparewith'] );
	$i = 0;
	
	foreach( $mimox_index as $rank => $current ) {
		$state = $current['diff'] > 0 ? 'state-increase' : ( $current['diff'] == 0 ? 'state-equal' : 'state-decrease' );
		$mimox_index_html .= '<span class="element"><span class="rank">'.$rank.'.</span> <span class="name">'.$current['name'].'</span> - <span class="value">'.$current['value'].'</span> <span class="state '.$state.'">'.$current['diff'].'</span></span>';
		if( $atts['limit'] > -1 ) {
			$i++;
			if( $atts['limit'] <= $i ) {
				break;
			}
		}
	}
	
	return '<div class="mimoxindex-slider-container" data-speed="'.$atts['speed'].'" data-direction="'.$atts['direction'].'" data-pauseonhover="'.$atts['pauseonhover'].'"><a class="prev"></a><p>'.$mimox_index_html.'</p><a class="next"></a></div>';
}

add_shortcode( 'mimox_mimoxindex', 'mimoxindex_shortcode_func' );

/* MIMOXINDEX shortcode END */

?>
