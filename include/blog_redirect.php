<?php
/**
 * Add new rewrite rule
 * These are deprecated since did not allowed to edit post url. 
 * /blog/%post_name%/ has to be set as custom url structure
 * CPTs rewrite property must contain: 'with_front'=>false ( 'rewrite' => array('slug' => 'jobs', 'with_front'=>false) )
 */
 /*
function create_new_url_querystring() {
    add_rewrite_rule(
        '([a-z]{2})\/*blog/([^/]*)$',
        'index.php?name=$matches[2]',
        'top'
    );
    add_rewrite_tag('%blog%','([^/]*)');
}
add_action('init', 'create_new_url_querystring', 999 );
*/
/**
 * Modify post link
 * This will print /blog/post-name instead of /post-name
 */
 /*
function append_query_string( $url, $post, $leavename ) {
	if ( $post->post_type == 'post' ) {		
		$url = home_url( user_trailingslashit( "blog/$post->post_name" ) );
	}
	return $url;
}
add_filter( 'post_link', 'append_query_string', 10, 3 );
*/
?>