<?php
// don't load directly
if (!defined('ABSPATH')) die('-1');
class VCTeamMemberClass {
    function __construct() {
        // We safely integrate with VC with this hook
        add_action( 'init', array( $this, 'integrateWithVC' ) );
 
        // Use this when creating a shortcode addon
        add_shortcode( 'mimox_team_member', array( $this, 'mimox_team_member_shortcode_function' ) );

        // Register CSS and JS
        add_action( 'wp_enqueue_scripts', array( $this, 'loadCssAndJs' ) );
    }

    public function loadCssAndJs() {}
 
    public function integrateWithVC() {
        // Check if Visual Composer is installed
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            // Display notice that Visual Compser is required
            add_action('admin_notices', array( $this, 'showVcVersionNotice' ));
            return;
        }

        /*
        Add your Visual Composer logic here.
        Lets call vc_map function to "register" our custom shortcode within Visual Composer interface.

        More info: http://kb.wpbakery.com/index.php?title=Vc_map
        */
        vc_map( array(
            "name" => __("Mimox team member", 'Mimox'),
            "description" => __("Show your team members", 'Mimox'),
            "base" => "mimox_team_member",
            "class" => "",
            "controls" => "full",
            "category" => __('Mimox shortcodes', 'js_composer'),
            "icon" => 'vc_extend_mimox', // or css class name which you can reffer in your css file later. Example: "vc_extend_my_class"
            "params" => array(
                array(
                  "type" => "wp_users",
                  "holder" => "div",
                  "class" => "",
                  "heading" => __("Select user", 'Mimox'),
                  "param_name" => "user",
                  "value" => '',
                  "description" => __("Select WordPress user to show as a Team Member", 'Mimox'),
              ),
                array(
                  "type" => "checkbox",
                  "holder" => "div",
                  "class" => "",
                  "heading" => __("Show long description", 'Mimox'),
                  "param_name" => "show_long_description",
                  "value" => '',
                  "description" => __("Show long description", 'Mimox'),
              ),
            )
        ) );
    }
    
    /*
    Shortcode logic how it should be rendered
    */
    public function mimox_team_member_shortcode_function( $atts, $content = null ) {
		extract( shortcode_atts( array(
		'user' => false,
		'show_long_description' => false,
		), $atts ) );
		global $authordata;
		$ad = $authordata;
		$authordata = get_userdatabylogin( $atts['user'] );
		
		global $show_long_description;
		$show_long_description = $atts['show_long_description'] ? true : false;
		
		ob_start();
      	us_load_template( 'templates/blog/single-post-author' );
      	$authordata = $ad;
      	
      	return '<div class="mimox-team-member">'.ob_get_clean().'</div>';
    }

    /*
    Show notice if your plugin is activated but Visual Composer is not
    */
    public function showVcVersionNotice() {
        $plugin_data = get_plugin_data(__FILE__);
        echo '
        <div class="updated">
          <p>'.sprintf(__('<strong>%s</strong> requires <strong><a href="http://bit.ly/vcomposer" target="_blank">Visual Composer</a></strong> plugin to be installed and activated on your site.', 'vc_extend'), $plugin_data['Name']).'</p>
        </div>';
    }
}

/*
Custom VC param to show available WordPress users
*/    
vc_add_shortcode_param( 'wp_users', 'mimox_wp_users_vc_param_function' );
function mimox_wp_users_vc_param_function( $settings, $value ) {
	$args = array(
		'role'         => '',
		'role__in'     => array(),
		'role__not_in' => array(),
		'orderby'      => 'display_name',
		'order'        => 'ASC',
	 ); 
	$users = get_users( $args );
	
	foreach( $users as $user ) {
		$users_select_options .= '<option value="'.$user->user_login.'" '.(  $value == $user->user_login ? 'selected' : '' ).'>'.$user->display_name.' ('.$user->user_email.')'.'</option>';
	}

	return '<select class="wpb_vc_param_value wpb-textinput ' .
             esc_attr( $settings['param_name'] ) . ' ' .
             esc_attr( $settings['type'] ) . '_field" name="' . esc_attr( $settings['param_name'] ) . '">'.$users_select_options.'</select>';
}

// Finally initialize code
new VCTeamMemberClass();
?>
