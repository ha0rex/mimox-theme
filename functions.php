<?php
show_admin_bar(false);
/* ini_set('display_errors', 1); */
/**
 * Loads the child theme textdomain.
 */
function mimox_child_theme_setup() {
    load_child_theme_textdomain( 'Mimox', plugin_dir_path(__FILE__) . '/languages' );
}
add_action( 'after_setup_theme', 'mimox_child_theme_setup' );

/**
 * Loads the parent theme (Zephyr) textdomain.
 */
function zephyr_theme_setup(){
    load_theme_textdomain('us', get_template_directory() . '/framework/languages');
}
add_action('after_setup_theme', 'zephyr_theme_setup');

/** Add Viewport meta tag for mobile browsers */
 add_action( 'wp_head', 'add_viewport_meta_tag' );
 	function add_viewport_meta_tag() {
 	echo '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/> <!--320-->';
 }

require_once 'include/shortcodes.php';

require_once 'include/job_offers_cpt.php';

require_once 'include/custom_user_fields.php';

require_once 'include/vc-team-member.php';

require_once 'include/settings_page.php';

require_once 'include/vc-form-lightbox.php';

/**
 * Rewrite blog posts url to /blog/${post-slug}
 */
require_once 'include/blog_redirect.php';

require_once 'include/wpcf7_job_offers_select.php';

if( is_admin() ) {
	wp_enqueue_style( 'mimox-admin', get_stylesheet_directory_uri().'/css/admin.css', array(), false, 'all' );
}

/**
 * Loads Mimox CSS styles for each device
 */
function mimox_styles_func() {
	wp_enqueue_style( 'mimox-style-desktop', get_stylesheet_directory_uri().'/css/desktop.css', array(), false, '(min-width: 1000px)' );
	wp_enqueue_style( 'mimox-style-mobile', get_stylesheet_directory_uri().'/css/mobile.css', array(), false, '(max-width: 1000px)' );
}
add_action( 'wp_enqueue_scripts', 'mimox_styles_func', 120 );

/**
 * Loads Mimox JS scripts.
 */
function mimox_scripts_func() { 
	wp_register_script( 'mimox-functions', get_stylesheet_directory_uri().'/js/functions.js' );

	// Localize the script with new data
	$translation_array = array(
		'Upload' => __( 'Upload', 'Mimox' ),
		'Tooltip' => __( 'Allowed file types: PDF, DOC, DOCX, ODT', 'Mimox' ),
	);
	wp_localize_script( 'mimox-functions', 'Mimox', $translation_array );
	
	wp_enqueue_script( 'mimox-functions', '', array(), false, true );
    wp_enqueue_script( 'mimox-net-calculator', get_stylesheet_directory_uri().'/js/mimox-net-calculator.js', array(), false, true ); 
    wp_enqueue_script( 'jquery-marquee', get_stylesheet_directory_uri().'/js/jquery.marquee.min.js', array(), false, true ); 
    wp_enqueue_script( 'snap-svg', get_stylesheet_directory_uri().'/assets/snapsvg/snap.svg-min.js', array(), false, true ); 
}
add_action( 'wp_enqueue_scripts', 'mimox_scripts_func' );

/**
 * Register Mimox customers sidebar and widgetized area.
 *
 */
function mimox_customers_left_sidebar_function() {
	register_sidebar( array(
		'name'          => 'Customers left sidebar',
		'id'            => 'customers_left_sidebar',
		'before_widget' => '<div class="mimox-customers-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'mimox_customers_left_sidebar_function', 98 );

/**
 * Register Mimox Geekhub sidebar and widgetized area.
 *
 */
function mimox_geekhub_left_sidebar_function() {
	register_sidebar( array(
		'name'          => 'Geekhub left sidebar',
		'id'            => 'geekhub_left_sidebar',
		'before_widget' => '<div class="mimox-customers-menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'mimox_geekhub_left_sidebar_function', 98 );

/**
 * Register Mimox Job offer list sidebar and widgetized area.
 *
 */
function mimox_job_offers_left_sidebar_function() {
	register_sidebar( array(
		'name'          => 'Job offers left sidebar',
		'id'            => 'job_offers_left_sidebar',
		'before_widget' => '<div class="mimox-job-offers-sidebar">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'mimox_job_offers_left_sidebar_function', 98 );

/**
 * Register Mimox after blog widget area
 *
 */
function mimox_after_blog_widget_area_function() {
	register_sidebar( array(
		'name'          => 'After blog widget area',
		'id'            => 'after_blog_widget_area',
		'before_widget' => '<div class="mimox-after-blog-widget-area">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'mimox_after_blog_widget_area_function', 98 );

/**
 * Place Mimox after blog widget area after the Blog list & Single blog post elements
 *
 */
 
function mimox_place_after_blog_widget_area_function() {
	return dynamic_sidebar( 'after_blog_widget_area' );
}
 
add_action( 'us_after_single', 'mimox_place_after_blog_widget_area_function' );
add_action( 'us_after_index', 'mimox_place_after_blog_widget_area_function' );

//define('DISABLE_ULTIMATE_GOOGLE_MAP_API', true);

/*
 * Phantom load translations for further use
 *
 */

__('Gross', 'Mimox');
__('How much is in net?', 'Mimox');


/*
 * Mimoxindex updater
 *
 */
 
function mimox_index($interval = '1 day', $comparewith = '-1 month') {
 	$mimox_index = get_option('mimox_mimoxindex');
 	
 	if( $mimox_index['updated'] < strtotime( -$interval ) || !is_array( $mimox_index['value'] ) ) {
 		$current = json_decode(file_get_contents('http://www.mimoxindex.com/ajax_gethistory/'));
 		$old = json_decode(file_get_contents('http://www.mimoxindex.com/ajax_gethistory/?searchdate='.date('Y-m-d', strtotime( $comparewith ))));
 		
 		foreach( $current as $name => $value ) {
 			$newjson[$value[1]] = array( 'rank'=> $value[1],'name'=> $name, 'value' => $value[0], 'diff' => $value[1]-$old->{$name}[1] );
 		}
 		
 		ksort($newjson);
 		
 		update_option('mimox_mimoxindex', array( 'updated' => time(), 'value' => $newjson ));
 		return $newjson;
 	}
 	else {
 		return $mimox_index['value'];
 	}
 }
 
/*
 * Removes Visual Composer admin menu item
 *
 */
 /*
function custom_menu_page_removing() {
    remove_menu_page('vc-welcome'); //vc
}
add_action( 'admin_init', 'custom_menu_page_removing' );
*/
/*
 * Change permission of Loco Translate from "update_options" to "edit_posts", so anyone can use the translation, who has the permission to edit a post
 *
 */
add_filter('loco_admin_capability', function( $capability ){
    return 'edit_posts';
} );

define( 'WPCF7_ADMIN_READ_CAPABILITY', 'edit_others_pages' );
define( 'WPCF7_ADMIN_READ_WRITE_CAPABILITY', 'edit_others_pages' );

function generateRandomString()
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randstring = '';
    for ($i = 0; $i < 10; $i++) {
        $randstring = $characters[rand(0, strlen($characters))];
    }
    return $randstring;
}