<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
/**
 * The template for displaying all single posts and attachments
 */
$us_layout = US_Layout::instance();
// Needed for canvas class
//$us_layout->titlebar = 'default';
get_header();

$template_vars = array(
	'metas' => (array) us_get_option( 'post_meta', array() ),
	'show_tags' => in_array( 'tags', us_get_option( 'post_meta', array() ) ),
);
?>
<!-- MAIN -->
<div class="l-main">
	<div class="l-main-h i-cf">

		<main class="l-content" itemprop="mainContentOfPage">

			<?php do_action( 'us_before_single_job_offer' ) ?>

			<?php
			while ( have_posts() ){
				the_post();

				$job_offer_details = get_post_meta( $post->ID, 'job_offer_details', true );
				
				$dress_code_translations = array( 'tie' => __('Tie', 'Mimox'), 'shirt' => __('Shirt', 'Mimox'), 't-shirt' => __('T-shirt', 'Mimox') );
				$expected_salary_period_translations = array( 'per-year' => __('/year', 'Mimox'), 'per-month' => __('/month', 'Mimox') );
				
				?>
				<article <?php post_class( 'l-section' ) ?>>
					<div class="l-section-h i-cf">
						<div class="w-blog">
							<div class="job-offer-list-sidebar">
								<?php echo get_all_job_offers( false, $post->ID ) ?>
								<div class="send-your-cv-now">
									<h3><?php _e('Haven\'t you found the right position for you?', 'Mimox') ?></h3>
									<p><?php _e('Send us your CV. You don\'t have to register, remember a password or fill in a lot of fields, just send, and it\'s done.', 'Mimox') ?></p>
									<?php echo do_shortcode('[cl-popup show_on="text" btn_label="'.__('I send it now', 'Mimox').'" align="left" size="l" paddings="" animation="fadeIn"][contact-form-7 id="'.$forms['CV'][get_locale()].'" title="Upload CV HU"][/cl-popup]') ?>
								</div>
							</div>
							<div class="job-container">
								<div class="job-header">
									<h2 class="title"><?php the_title() ?></h2>
									<div class="salary">
										<label><?php _e('Gross', 'Mimox') ?></label>
										<div class="expected-salary">
											<span class="expected-salary-from"><?php echo $job_offer_details['expected-salary-from'] ?></span>
											<span class="separator">-</span>
											<span class="expected-salary-to"><?php echo $job_offer_details['expected-salary-to'] ?></span>
											<span class="expected-salary-currency">&nbsp;<?php echo $job_offer_details['expected-salary-currency'] ?></span>
											<span class="expected-salary-period"><?php echo $expected_salary_period_translations[$job_offer_details['expected-salary-period']] ?></span>
											<br />
											<div class="expected-salary-extra"><?php echo $job_offer_details['expected-salary-extra'] ?></div>
										</div>
										<?php /* echo do_shortcode('[formlightbox_call title="" class="calculator'.$post->ID.' w-btn style_raised color_primary"]'.__('How much is in net?', 'Mimox').'[/formlightbox_call]'); */ ?>
										<div class="calculator-trigger">
											<?php echo do_shortcode('[cl-popup btn_label="'.__('How much is in net?', 'Mimox').'" align="center" size="l" paddings="" animation="fadeIn"]<div class="lightbox-calculator"><h3>'.__('How much is in net?', 'Mimox').'</h3>[mimox_calculator currency="'.$job_offer_details['expected-salary-currency'].'" no_of_children_min="0" no_of_children_max="10" no_of_children_default="" gross_salary_default="'.$job_offer_details['expected-salary-to'].'"]<a href="#" class="close w-btn style_raised color_primary">'.__('Close', 'Mimox').'</a></div>[/cl-popup]') ?>
										</div>
									</div>
									<div class="benefits">
										<label><?php _e('Benefits', 'Mimox') ?></label>
										<?php echo $job_offer_details['benefits'] ?>
									</div>
									<div class="dress-code">
										<img title="<?php echo $dress_code_translations[$job_offer_details['dress-code']] ?>" src="<?php echo get_stylesheet_directory_uri().'/images/'.$job_offer_details['dress-code'].'.png' ?>" />
									</div>
								</div>
									<div class="job-excerpt-mobile">
										<div class="job-place-of-work">
											<?php echo $job_offer_details['place-of-work'] ?>
										</div>
										<div class="job-for-you-if">
											<?php echo $job_offer_details['for-you-if'] ?>
										</div>
									</div>
								<div class="job-body">
									<div class="job-description">
										<?php the_content() ?>
									</div>
									<div class="job-requirements">
										<?php echo $job_offer_details['requirements'] ?>
									</div>
									<div class="job-apply-box">
										<h3><?php _e('Do you like the job?', 'Mimox') ?></h3>
										<?php $forms = get_option('mimox_forms'); ?>
										<?php /* echo do_shortcode('[formlightbox_call title="" class="apply'.$post->ID.' w-btn style_raised color_primary"]'.__('Apply now!', 'Mimox').'[/formlightbox_call]'); */ ?>
										<?php echo do_shortcode('[cl-popup btn_label="'.__('Apply now!', 'Mimox').'" align="center" size="l" paddings="" animation="fadeIn"]<div class="lightbox-job-apply">[contact-form-7 id="'.$forms['job_apply'][get_locale()].'" job_offer_id="<?php echo $post->ID ?>" title="1 kapcsolati űrlap"]</div>[/cl-popup]') ?>
									</div>
								</div>
								<div class="job-sidebar">
									<div class="job-excerpt-desktop">
										<div class="job-place-of-work">
											<?php echo $job_offer_details['place-of-work'] ?>
										</div>
										<div class="job-for-you-if">
											<?php echo $job_offer_details['for-you-if'] ?>
										</div>
									</div>
									<div class="job-author">
										<h3 class="title"><?php _e('If you have a question, write to me!', 'Mimox') ?></h3>
										<?php us_load_template( 'templates/blog/single-post-author' ) ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</article>
				<?php
			}
			
			?>
			<?php /* echo do_shortcode('[formlightbox_obj id="calculator'.$post->ID.'" style="" onload="false"][/formlightbox_obj]') */ ?>
			<?php /* echo do_shortcode('[formlightbox_obj id="sendCV" style="" onload="false"]<div class="lightbox-calculator">[contact-form-7 id="'.$forms['CV'][get_locale()].'" title="Upload CV HU"]</div>[/formlightbox_obj]') */ ?>

			<?php do_action( 'us_after_single_job_offer' ) ?>

		</main>

		<?php if ( $us_layout->sidebar_pos == 'left' OR $us_layout->sidebar_pos == 'right' ): ?>
			<aside class="l-sidebar at_<?php echo $us_layout->sidebar_pos ?>" itemscope="itemscope" itemtype="https://schema.org/WPSideBar">
				<?php generated_dynamic_sidebar('default_sidebar'); ?>
			</aside>
		<?php endif; ?>

	</div>
</div>

<?php get_footer(); ?>
