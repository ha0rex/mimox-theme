function numberWithCommas(x) {
    return x.toString().replace(/ /g, '').replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}

jQuery(document).ready(function() {
	/*
	 * Prevent scroll to top when a lightbox caller is clicked
	 */
	jQuery('.cboxElement').each(function() {
		jQuery(this).attr('href', null);
	});
	
	/*
	 * Create stylized file uploads
	 */
	jQuery('input[type=file]').each(function() {
		var that = this;
		var container = jQuery('<div class="file-upload-container"><a class="w-btn style_raised color_primary upload">'+Mimox.Upload+'</a><span class="tooltip">'+Mimox.Tooltip+'</span><span class="filename"></span></div>');
		container.insertAfter(jQuery(this));
		jQuery('.upload', container).click(function() {
			jQuery(that).trigger('click');
		});
		jQuery(this).change(function() {
			var filename = jQuery(this).val().split('\\');
			jQuery('.filename', container).html(filename[filename.length-1]);
		});
	});	
	
	jQuery(document).on('mailsent.wpcf7', function (e) {
		jQuery('.position', e.target).html(jQuery('select[name=position] option:selected').val());
		jQuery('.step-active', e.target).addClass('step-success');
		
		jQuery('.close', e.target).click(function() {
			/* jQuery('#cboxClose').trigger('click'); */
			jQuery('.cl-popup-closer').trigger('click');
		});
		jQuery(e.target).addClass('emailsent');
	});
	
	jQuery('.cl-popup-box-h .close').click(function() {
		jQuery('.cl-popup-closer').trigger('click');
	});
	
	jQuery(document).on('invalid.wpcf7', function (e) {
	});
	
	jQuery('.mimox-customers-menu ul .current-menu-item > a').click(function() {
		var menu = jQuery('.mimox-customers-menu');
		if( !menu.hasClass('open') ) {
			menu.addClass('open');
		}	
		else {
			menu.removeClass('open');
		}
		return false;
	});
	
	jQuery('.job-offer-list .current').click(function() {
		var menu = jQuery('.job-offer-list');
		if( !menu.hasClass('open') ) {
			menu.addClass('open');
		}	
		else {
			menu.removeClass('open');
		}
		
		jQuery(window).click(function() {
			menu.removeClass('open');
		});
		jQuery(menu).click(function(e) {
			e.stopPropagation();
		});
		return false;
	});
	
	jQuery('.mimoxindex-slider-container').each(function() {
		var container = jQuery(this);
		jQuery(this).marquee({
			pauseOnHover: container.attr('pauseonhover'),
			speed: container.attr('data-speed'),
			direction: container.attr('data-direction'),
		}); 
	});

	jQuery('.mimoxindex-slider-container .next, .mimoxindex-slider-container .prev').click(function() {
		return false;
	});
});