function init_mimox_net_calculator() {
	jQuery(document).ready(function() {
		jQuery('.mimox-form-calculator .calculate').click(function() {
			var gross_salary = parseInt( jQuery(this).closest('.mimox-form-calculator').find('input[name="gross_salary"]').val().replace(/ |\./g, '') );
			var no_of_children = jQuery(this).closest('.mimox-form-calculator').find('input[name="no_of_children"]').val();
			no_of_children = no_of_children ? no_of_children : 0;
			var net_salary_formula = jQuery(this).closest('.mimox-form-calculator').attr('data-formula').replace('gross_salary', gross_salary).replace('no_of_children', no_of_children);
			var currency = jQuery(this).closest('.mimox-form-calculator').attr('data-currency');
			var result_value_container = jQuery(this).closest('.mimox-form-calculator').find('.result .value');
			var result_container = jQuery(this).closest('.mimox-form-calculator').find('.result');
		
			if( gross_salary > 0 && no_of_children % 1 === 0 ) {
				result_value_container.html(numberWithCommas(Math.round(eval('(function() {' + net_salary_formula + '}())')))+' '+currency);
				result_container.css('opacity', 1);
			}
			else {
				if( gross_salary < 0 || gross_salary % 1 !== 0 ) {
					jQuery(this).closest('.mimox-form-calculator').find('input[name="gross_salary"]').addClass('error');
				}
				if( no_of_children % 1 !== 0 ) {
					jQuery(this).closest('.mimox-form-calculator').find('input[name="no_of_children"]').addClass('error');
				}
			}
			return false;
		});
	
		jQuery('.mimox-form-calculator input[name="gross_salary"]').keyup(function() {
			jQuery(this).val( numberWithCommas( jQuery(this).val() ) );
			jQuery(this).removeClass('error');
		});
		
		jQuery('.mimox-form-calculator input[name="no_of_children"]').keyup(function() {
			jQuery(this).removeClass('error');
		});
	});
}